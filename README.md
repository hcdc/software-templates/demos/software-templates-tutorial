# Enhancing Research Software Sustainability through Modular Open-Source Software Templates

**deRSE24 -- Conference for Research Software Engineering in Germany**

March 7th, 2024

Philipp S. Sommer, Björn Saß, Markus Benninghoff

[![Binder](https://mybinder.org/badge_logo.svg)][mybinder]

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11320120.svg)](https://doi.org/10.5281/zenodo.11320120)

## Preface

This tutorial on software templates is presented at the 
[deRSE24 conference][derse24], but it is designed such that you can go through 
the tutorial yourself without a trainer.

[derse24]: https://events.hifis.net/event/994/contributions/7994/

## Abstract

Research software development is crucial for scientific advancements, yet the 
sustainability and maintainability of such software pose significant 
challenges. In this tutorial, we present a comprehensive demonstration on 
leveraging software templates to establish best-practice implementations for 
research software, aiming to enhance its longevity and usability.

Our approach is grounded in the utilization of Cookiecutter, augmented with a 
fork-based modular Git strategy, and rigorously unit-tested methodologies. By 
harnessing the power of Cookiecutter, we streamline the creation process of 
research software, providing a standardized and efficient foundation. The 
fork-based modular Git approach enables flexibility in managing variations, 
facilitating collaborative development while maintaining version control and 
traceability.

Central to our methodology is the incorporation of unit testing, ensuring code 
integrity and reliability of the templates. Moreover, we employ Cruft, a tool
tailored to combat the proliferation of boilerplate code, often referred to as
the "boilerplate-monster." By systematically managing and removing redundant 
code, Cruft significantly enhances the maintainability and comprehensibility of 
research software. This proactive approach mitigates the accumulation of 
technical debt and facilitates long-term maintenance.

The open-source templates are available at 
https://codebase.helmholtz.cloud/hcdc/software-templates/. In the first 30 
minutes of the tutorial, participants will gain insights into the structured 
organization of these software templates, enabling them to understand the 
framework's architecture and application to their own software products. The 
subsequent 30 minutes will be dedicated to a hands-on tutorial, allowing 
participants to engage directly with the templates, guiding them through the 
process of implementing and customizing them for their specific research 
software projects.

Maintaining research software presents distinct challenges compared to 
traditional software development. The diverse skill sets of researchers, time 
constraints, lack of standardized practices, and evolving requirements 
contribute to the complexity. Consequently, software often becomes obsolete, 
challenging to maintain, and prone to errors.

Through our tutorial, we address these challenges by advocating for the 
adoption of software templates. These templates encapsulate best practices, 
enforce coding standards, and promote consistent structures, significantly 
reducing the cognitive load on developers. By providing a well-defined starting 
point, researchers can focus more on advancing their scientific endeavors 
rather than grappling with software complexities.

Furthermore, the utilization of software templates fosters collaboration and 
knowledge sharing within research communities. It encourages the reuse of 
proven solutions, accelerates the onboarding process for new contributors, and 
facilitates better documentation practices. Ultimately, this approach leads to 
a more sustainable ecosystem for research software, fostering its evolution and 
ensuring its relevance over time.

In summary, our tutorial offers a practical and comprehensive guide to creating 
and utilizing software templates for research software development. By 
harnessing Cookiecutter with Git-based modularity, unit testing, and the power 
of Cruft, we aim to empower researchers in building robust, maintainable, and 
sustainable software, thereby advancing scientific progress in an efficient and 
impactful manner.

## Scope of this tutorial

In this tutorial, you will learn the basics and ideas on the workflow that is 
behind the HCDC software templates. It is not intended for users that just want 
to use and understand a specific template.


## About this repository

This presentation is a jupyter notebook presented with [RISE][rise]. You can
access the raw notebook at
[software-templates-tutorial.ipynb](software-templates-tutorial.ipynb).

You can also execute the cells in this presentation interactively by clicking
on

[![Binder](https://mybinder.org/badge_logo.svg)][mybinder]

To read more about the navigation in a rise slideshow, please
[read the docs][rise-usage]

[mybinder]: https://mybinder.org/v2/git/https%3A%2F%2Fcodebase.helmholtz.cloud%2Fhcdc%2Fsoftware-templates%2Fdemos%2Fsoftware-templates-tutorial/main?filepath=software-templates-tutorial.ipynb
[rise-usage]: https://rise.readthedocs.io/en/latest/usage.html#running-a-slideshow

## Usage without binder

This repo is binder-ready, meaning that you can run the entire tutorial online
for free on mybinder.org by clicking the following badge

[![Binder](https://mybinder.org/badge_logo.svg)][mybinder]

However, binder has certain limitations, especially when it comes to the 
persistence of data (your sessions on mybinder.org will be deleted after a
certain timespan of inactivity). But there are alternatives that you can use:
you can just use your local setup (requires linux and conda), or use a 
docker-based setup (requires [docker][docker] or [podman][podman]).

[docker]: https://docs.docker.com/engine/install/
[podman]: https://podman.io/docs/installation

### Running locally with conda and linux

If you are having a linux computer with [conda][miniconda] installed, you can
spawn a jupyter notebook server locally and run the tutorial from your 
localhost.

1. clone the source code of the repo and cd into it

   ```bash
   git clone https://codebase.helmholtz.cloud/hcdc/software-templates/demos/software-templates-tutorial.git
   cd software-templates-tutorial
   ```
2. create the conda environment

   ```bash
   conda env create -f binder/environment.yml
   ```

3. activate it

   ```bash
   conda activate software-templates-tutorial
   ```
4. start a jupyter notebook server via

   ```bash
   jupyter lab
   ```

[miniconda]: https://docs.anaconda.com/free/miniconda/

### Running locally with docker

We are building the docker image using [repo2docker][repo2docker] in our
Continuous Integration pipeline and deploy it in the container registry of
gitlab. Hence, if you have docker installed, you can simply create a container
from this image via

```bash
docker run -p 8888:8888 --rm registry.hzdr.de/hcdc/software-templates/demos/software-templates-tutorial:latest
```

and visit the last link (something like http://127.0.0.1:8888/?token=...) in
your browser.

### Running locally with docker-compose

You can also the docker image that we built via docker-compose. 

1. Save the following config to a file name `compose.yml`

   ```yaml
   version: '3'
   services:
     software-templates-tutorial:
       container_name: software-templates-tutorial
       image: "registry.hzdr.de/hcdc/software-templates/demos/software-templates-tutorial:latest"
       restart: unless-stopped
       ports:
         - "8888:8888"
   ```
2. run `docker compose up`
3. visit the last link shown to you (something like 
   http://127.0.0.1:8888/?token=...) in your browser

### Build and run locally with repo2docker

Instead of using our free image from the registry at registry.hzdr.de, you can
also build the image yourself using `repo2docker`. Just install 
[jupyter-repo2docker][repo2docker] and run

```bash
jupyter-repo2docker https://codebase.helmholtz.cloud/hcdc/software-templates/demos/software-templates-tutorial.git
```

This will build an image and start a container for you.

[repo2docker]: https://pypi.org/project/jupyter-repo2docker/

### Usage with jupyterhub

If you have a jupyterhub available and can configure your own images, you can
also use our image from `registry.hzdr.de/hcdc/software-templates/demos/software-templates-tutorial:latest`. Just add the following lines to the config
for your JupyterHub:

```python
c.DockerSpawner.image = 'registry.hzdr.de/hcdc/software-templates/demos/software-templates-tutorial:latest'
c.DockerSpawner.cmd = ['jupyterhub-singleuser']
```

See https://repo2docker.readthedocs.io/en/latest/howto/jupyterhub_images.html
for more information on this.

## License

The contents of this repository is published under the Creative Commons
Attribution 4.0 International Public License (CC BY 4.0).

See the [LICENSE](LICENSE) file for more details.

Copyright (c) 2022-2024, Helmholtz-Zentrum hereon GmbH
